﻿/*
 * Name: Hiren Desai
 * Student Id: 8716470
 * Section: Sec 5
 * Purpose: Midterm
 * 
*/

using System;
using System.Drawing;
using System.Windows.Forms;

namespace HDMidterm
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        //define varibale
        Image i;
        ImageList imageList = new ImageList();
        int count = 0;

        //show manual buttons
        public void showManualBtns()
        {
            firstBtn.Visible = true;
            previousBtn.Visible = true;
            nextBtn.Visible = true;
            lastBtn.Visible = true;

            playBtn.Visible = false;
            stopBtn.Visible = false;
        }

        //show automatic buttons
        public void showAutomaticBtns()
        {
            playBtn.Visible = true;
            stopBtn.Visible = true;

            firstBtn.Visible = false;
            previousBtn.Visible = false;
            nextBtn.Visible = false;
            lastBtn.Visible = false;
        }

        //manual button click
        private void manualBtn_Click(object sender, EventArgs e)
        {
            showManualBtns();
        }

        //automatic button click
        private void automaticBtn_Click(object sender, EventArgs e)
        {
            showAutomaticBtns();
        }

        //load image from system to imagelist
        private void loadImagesBtn_Click(object sender, EventArgs e)
        {
            imageList.ColorDepth = ColorDepth.Depth32Bit;
            imageList.ImageSize = new Size(256, 256); //actual size of image

            OpenFileDialog open = new OpenFileDialog();
            DialogResult dialogResult = open.ShowDialog();

            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            if (dialogResult == DialogResult.OK)
            {
                i = Image.FromFile(open.FileName);
                Bitmap b = (Bitmap)i;
                imageList.Images.Add(b);

                string title = "Title";
                string message = "Image Added \n Total Images added so far:" + imageList.Images.Count;
                MessageBox.Show(message, title);
            }
        }

        //previous button click
        private void previousBtn_Click(object sender, EventArgs e)
        {
            //if count is greater than zero 
            if (count > 0)
                count--;
            pictureBox.Image = imageList.Images[count];
        }

        //next button click
        private void nextBtn_Click(object sender, EventArgs e)
        {
            if (count < imageList.Images.Count - 1)
                count++;
            pictureBox.Image = imageList.Images[count];
        }

        //first button click
        private void firstBtn_Click(object sender, EventArgs e)
        {
            pictureBox.Image = imageList.Images[count];
        }

        //last button click
        private void lastBtn_Click(object sender, EventArgs e)
        {
            pictureBox.Image = imageList.Images[imageList.Images.Count - 1];
        }

        //play button click
        private void playBtn_Click(object sender, EventArgs e)
        {
            //set timer interval
            timer.Interval = 800;

            //start the timer 
            timer.Start();
        }

        //stop button click
        private void stopBtn_Click(object sender, EventArgs e)
        {
            //timer stop
            timer.Stop();
        }

        //timer tick 
        private void timer_Tick(object sender, EventArgs e)
        {
            //load image slider images
            loadNextImage();
        }

        //load image method
        private void loadNextImage()
        {
            if (count < imageList.Images.Count - 1)
                count++;
            pictureBox.Image = imageList.Images[count];
        }

    }
}
